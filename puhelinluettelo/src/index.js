import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import henkilot from './henkilot.json'

const Table = () => {
    const otsikot = [{ teksti: 'Etunimi', key: '82' }, { teksti: 'Sukunimi', key: '83' }]; 


    return(
    <div>
      <table>
        <thead>
        <tr>
         {Object.keys(henkilot[0]).map((otsikko, index) => (
             <th key={index}>{otsikko}</th>
         ))}
        </tr>
      </thead>
      <tbody>
            {henkilot.map((henkilo, index) => 
                 (<tr key={index}>{Object.values(henkilo).map((tieto, index) =>  
                   (<td key={index}>{tieto}</td>))}</tr>)
                )}
           
      </tbody>
      </table>
    </div>
    )
}

  const App = () => <Table />;
  
ReactDOM.render(<App />, document.getElementById('root'));